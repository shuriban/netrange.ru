<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<h2>Контакты</h2>
			<hr>
			</div>
			<div class="col-md-4 col-md-offset-4">
				<form action="/contact.php" id="contact-form" class="form-horizontal" method="POST">
					<h3 class="text-center">Форма обратной связи</h3>
					<fieldset>
						<div class="form-group">
							<input type="text" placeholder="Ваше имя" class="form-control" name="name" id="name">
						</div>
						<div class="form-group">
							<input type="text" placeholder="Введите свой Email" class="form-control" name="email" id="email">
						</div>
						<div class="form-group">
							<input type="text" placeholder="Тема сообщения" class="form-control" name="subject" id="subject">
						</div>
						<div class="form-group">
							<textarea placeholder="напишите ваш вопрос или сообщение" class="form-control" name="message" id="message" rows="3"></textarea>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-success g-recaptcha">Отправить</button>
							<button type="reset" class="btn btn-primary">Отмена</button>
						</div>
					</fieldset>
				</form>
				
				
				<div class="text-center">
				
				<h4>Также со мной можно связаться:</h4>
					<ul class="list-inline">
						<li><a href="https://www.facebook.com/shuriban90" target="_blank" rel="nofollow" title=" facebook"><i class="fa fa-facebook fa-3x"></i></a></li>
						<li><a href="https://vk.com/gruzdev_aleksandr" target="_blank" title="Вконтакте" rel="nofollow"><i class="fa fa-vk fa-3x"></i></a></li>
						<li><a href="https://ok.ru/shuriban" target="_blank" title="Одноклассники" rel="nofollow"><i class="fa fa-odnoklassniki fa-3x"></i></a></li>
						<li><a href="https://plus.google.com/u/0/+%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%BD%D0%B4%D1%80%D0%93%D1%80%D1%83%D0%B7%D0%B4%D0%B5%D0%B2" target="_blank" rel="nofollow" title="Гугл+"><i class="fa fa-google-plus fa-3x"></i></a></li>
						<br>
						<li><a href="tel:+79505719137"><i class="fa fa-mobile fa-3x"></i></a></li>
						<li><a href="skype:Shuriban90?call"><i class="fa fa-skype fa-3x"></i></a></li>
						<li><a href="mailto:shuriban@mail.ru"><i class="fa fa-envelope-o fa-3x"></i></a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
</div>