<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<html>
	<head>
		<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
		<meta charset="<?echo LANG_CHARSET;?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?$APPLICATION->ShowMeta("keywords")?>
		<?$APPLICATION->ShowMeta("description")?>
		<meta name="Content-language" content="ru">
		<meta name="Content-type" content="text/html; charset=<?echo LANG_CHARSET;?>">
		<title><?$APPLICATION->ShowTitle();?></title>
		<?$APPLICATION->ShowHead();?>
		<?IncludeTemplateLangFile(__FILE__);?>
		<meta name="author" content="https://NetRange.ru">
		
		<link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css" rel="stylesheet">
		<link href="<?=SITE_TEMPLATE_PATH?>/css/slidefolio.css" rel="stylesheet">
		<link href="<?=SITE_TEMPLATE_PATH?>/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?=SITE_TEMPLATE_PATH?>/css/costom.css" rel="stylesheet">
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-scrolltofixed-min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.vegas.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mixitup.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.js"></script>
	</head>
	<body>
		<img class="vegas-background" style="position: fixed; left: 0px; top: 0px; width: 100%; height: 100%;" src="<?=SITE_TEMPLATE_PATH?>/images/blue_binary-code.jpg">
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
		<!--START top nav-->
		<div id="nav">
			<nav class="navbar navbar-new" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobilemenu">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="mobilemenu">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu", 
							"netrange", 
							Array(
								"ROOT_MENU_TYPE"	=>	"top",
								"MAX_LEVEL"	=>	"1",
								"USE_EXT"	=>	"N"
							)
						);?>
					</div> 
				</div>
			</nav>
		</div>
		<!--END top nav-->
		<!--START title-->
		<div class="body_title">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 vert-text">
						<h1><em><?$APPLICATION->ShowTitle(false)?></em></h1>
						<p class="lead" >
							<?$APPLICATION->ShowProperty('description');?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!--END title-->
		<!--START body_site-->
		<div class="body_site">
			<!--START container-->
			<div class="container">
				<!--START row-->
				<div class="row push50">
					<!--START col-md-8-->
					<div class="col-md-8">
						<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","netrange",Array(
								"START_FROM" => "0", 
								"PATH" => "", 
								"SITE_ID" => "s1" 
							)
						);?>
						<div class="well well-lg" id="post_full_story" >