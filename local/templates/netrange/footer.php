<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
						</div>
					</div>
					<!--END col-md-8-->
					<!--START Right_col_inc-->
					<div class="col-md-3 col-md-offset-1 well">
					<?/*
						$APPLICATION->IncludeFile(
						substr($APPLICATION->GetCurPage(), 0, strlen($APPLICATION->GetCurPage())-4)."_inc.php",
						Array(),
						Array("MODE"=>"html", "NAME"=>GetMessage("PAGE_INC"), "TEMPLATE"=>"right_col_inc.php"));
						*/
					?>
					<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "headerinc",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "right_col_inc.php",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
					</div>
				<!--END Right_col_inc-->
				</div>
				<!--END row-->
			</div>
			<!--END container-->
		</div>
		<!--END body_site-->
<?

$APPLICATION->IncludeFile(
	SITE_TEMPLATE_PATH."/include_areas/contact_form.php",
	Array(),
	Array("MODE"=>"html")
);

?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<em>Copyright &copy; Netrange.ru 2016г</em>
			</div>
		</div>
	</div>
</footer>
<?

$APPLICATION->IncludeFile(
	SITE_TEMPLATE_PATH."/include_areas/counters.php",
	Array(),
	Array("MODE"=>"html")
);

?>
<script type="text/javascript">$(document).ready(function(){$('#nav').scrollToFixed();});</script>
</html>